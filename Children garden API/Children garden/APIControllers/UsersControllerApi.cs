﻿using Children_garden.APIControllers.Helpers;
using Children_garden_Data.Exceptions;
using Children_garden_Data.Models.Dtos;
using Children_garden_Data.Models.QueryParams;
using Children_garden_Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Children_garden.APIControllers
{
    [Route("api/users")]
    [ApiController]
    public class UsersControllerApi : ControllerBase
    {
        private readonly IUsersService _usersService;
        private readonly AuthorizationHelper _authorizationHelper;

        public UsersControllerApi(IUsersService usersService, AuthorizationHelper helper)
        {
            _usersService = usersService;
            _authorizationHelper = helper;
        }

        [HttpGet("")]
        public IActionResult GetAllBy([FromQuery] UsersQueryParams usersQueryParams)
        {
            try
            {
                var users = _usersService.FilterBy(usersQueryParams);

                return Ok(users);
            }
            catch (EntityNotFoundException notFound)
            {
                return NotFound(notFound.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById([FromHeader] string username, string password, int id)
        {
            var user = this._authorizationHelper.TryGetUser(username, password);
            try
            {
                return Ok(await _usersService.GetByIdAsync(id));
            }
            catch (EntityNotFoundException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromHeader] string username, string password, [FromBody] UserCreateDto userToCreate)
        {
            try
            {
                var user = _authorizationHelper.TryGetUser(username, password);
                var createdUser = await this._usersService.CreateAsync(userToCreate);

                return Created($"User details were successfuly entered!", createdUser);
            }
            catch (InvalidInputException invalidInput)
            {
                return BadRequest(invalidInput.Message);
            }
            catch (UnauthorizedOperationException exception)
            {
                return Conflict(exception.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Update( [FromHeader]string username, string password, [FromBody] UserUpdateDto updateDto)
        {
            try
            {
                var user = _authorizationHelper.TryGetUser(username, password);

                var updatedUser = await this._usersService.UpdateAsync(updateDto);

                return Created($"User details were successfuly updated!", updatedUser);
            }
            catch (InvalidInputException invalidInput)
            {
                return BadRequest(invalidInput.Message);
            }
            catch (UnauthorizedOperationException exception)
            {
                return Conflict(exception.Message);
            }
        }


        [HttpDelete]
        public async Task<IActionResult> Delete([FromHeader] string username, string password, int id)
        {
            try
            {
                var user = _authorizationHelper.TryGetUser(username, password);

                await _usersService.DeleteAsync(id);

                return Ok($"User with ID: {id} was successfully deleted!");
            }
            catch (InvalidInputException invalidInput)
            {
                return BadRequest(invalidInput.Message);
            }
            catch (UnauthorizedOperationException exception)
            {
                return Conflict(exception.Message);
            }
            catch (EntityNotFoundException notFound)
            {
                return BadRequest(notFound.Message);
            }
        }
    }
}
