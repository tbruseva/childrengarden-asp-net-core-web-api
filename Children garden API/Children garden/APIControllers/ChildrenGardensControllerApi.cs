﻿using Children_garden.APIControllers.Helpers;
using Children_garden_Data.Exceptions;
using Children_garden_Data.Models.Dtos;
using Children_garden_Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Children_garden.APIControllers
{
    [Route("api/childrengardens")]
    [ApiController]
    public class ChildrenGardensControllerApi : ControllerBase
    {
        private readonly IChildrenGardensService _childrenGardensService;
        private readonly AuthorizationHelper _authorizationHelper;

        public ChildrenGardensControllerApi(IChildrenGardensService childrenGardensService, AuthorizationHelper authorizationHelper)
        {
            _childrenGardensService = childrenGardensService;
            _authorizationHelper = authorizationHelper;
        }

       
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById([FromHeader]int id)
        {
            try
            {
                return Ok(await _childrenGardensService.GetByIdAsync(id));
            }
            catch (EntityNotFoundException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] ChildrenGardenCreateDto gardenToCreate)
        {
            try
            {
                var username = HttpContext.User.Identity.Name;
                var createdGarden = await this._childrenGardensService.CreateAsync(gardenToCreate, username);
                return Created($"Garden with id {createdGarden.Id} was succesfully created!", createdGarden);
            }
            catch (InvalidInputException invalidInput)
            {
                return BadRequest(invalidInput.Message);
            }
            catch (UnauthorizedOperationException exception)
            {
                return Conflict(exception.Message);
            }
        }

        [HttpPut]
        //[Authorize(Roles = "Director"), Authorize(Roles = "Admin"), Authorize(Roles = "Teacher")]
        public async Task<IActionResult> Update([FromBody] ChildrenGardenUpdateDto gardenToUpdate)
        {
            try
            {
                var username = HttpContext.User.Identity.Name;
                var updatedGarden = await this._childrenGardensService.UpdateAsync(gardenToUpdate, username);

                return Created($"Garden details were successfuly updated!", updatedGarden);
            }
            catch (InvalidInputException invalidInput)
            {
                return BadRequest(invalidInput.Message);
            }
            catch (UnauthorizedOperationException exception)
            {
                return Conflict(exception.Message);
            }
        }

        //[Authorize(Roles = "Director"), Authorize(Roles = "Admin"), Authorize(Roles = "Teacher")]

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var username = HttpContext.User.Identity.Name;

                await _childrenGardensService.DeleteAsync(id, username);

                return Ok($"Garden with ID: {id} was successfully deleted!");
            }
            catch (InvalidInputException invalidInput)
            {
                return BadRequest(invalidInput.Message);
            }
            catch (UnauthorizedOperationException exception)
            {
                return Conflict(exception.Message);
            }
            catch (EntityNotFoundException notFound)
            {
                return BadRequest(notFound.Message);
            }
        }
    }
}
