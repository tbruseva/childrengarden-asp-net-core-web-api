﻿using Children_garden.APIControllers.Helpers;
using Children_garden_Data.Exceptions;
using Children_garden_Data.Models.Dtos;
using Children_garden_Data.Models.QueryParams;
using Children_garden_Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Children_garden.Controllers
{
    [ApiController]
    [Route("api/children")]
    public class ChildrenControllerApi : ControllerBase
    {
        private readonly IChildrenService _childrenService;
        private readonly AuthorizationHelper _authorizationHelper;

        public ChildrenControllerApi(IChildrenService childrenService, AuthorizationHelper helper)
        {
            _childrenService = childrenService;
            _authorizationHelper = helper;
        }

        [HttpGet("")]
        public IActionResult GetAllBy([FromQuery] ChildrenQueryParams childrenQueryParams)
        {
            try
            {
                var children = _childrenService.FilterBy(childrenQueryParams);

                return Ok(children);
            }
            catch (EntityNotFoundException notFound)
            {
                return NotFound(notFound.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById([FromHeader] string username, string password, int id)
        {
            var user = this._authorizationHelper.TryGetUser(username, password);
            try
            {
                return Ok(await _childrenService.GetByIdAsync(id));
            }
            catch (EntityNotFoundException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        //[Authorize(Roles = "Director"), Authorize(Roles = "Admin"), Authorize(Roles = "Teacher")]
        public async Task<IActionResult> Create([FromBody] ChildCreateDto childToCreate)
        {
            try
            {
                var username = HttpContext.User.Identity.Name;
                var createdChild = await this._childrenService.CreateAsync(childToCreate, username);
                return Created($"Child details were successfuly entered!", createdChild);
            }
            catch (InvalidInputException invalidInput)
            {
                return BadRequest(invalidInput.Message);
            }
            catch (UnauthorizedOperationException exception)
            {
                return Conflict(exception.Message);
            }
        }

        [HttpPut]
        //[Authorize(Roles = "Director"), Authorize(Roles = "Admin"), Authorize(Roles = "Teacher")]
        public async Task<IActionResult> Update([FromBody] ChildUpdateDto childToUpdate)
        {
            try
            {
                var username = HttpContext.User.Identity.Name;
                var updatedChild = await this._childrenService.UpdateAsync(childToUpdate, username);
                return Created($"Child details were successfuly updated!", updatedChild);
            }
            catch (InvalidInputException invalidInput)
            {
                return BadRequest(invalidInput.Message);
            }
            catch (UnauthorizedOperationException exception)
            {
                return Conflict(exception.Message);
            }
        }

        //[Authorize(Roles = "Director"), Authorize(Roles = "Admin"), Authorize(Roles = "Teacher")]

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var username = HttpContext.User.Identity.Name;

                await _childrenService.DeleteAsync(id, username );
                return Ok($"Child with ID: {id} was successfully deleted!");
            }
            catch (InvalidInputException invalidInput)
            {
                return BadRequest(invalidInput.Message);
            }
            catch (UnauthorizedOperationException exception)
            {
                return Conflict(exception.Message);
            }
            catch (EntityNotFoundException notFound)
            {
                return BadRequest(notFound.Message);
            }
        }

    }
}

