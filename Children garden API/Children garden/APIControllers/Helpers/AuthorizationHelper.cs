﻿using Children_garden_Data.Exceptions;
using Children_garden_Data.Models;
using Children_garden_Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Children_garden.APIControllers.Helpers
{
    public class AuthorizationHelper
    {
        private readonly IUsersService _usersService;

        public AuthorizationHelper(IUsersService usersService)
        {
            this._usersService = usersService;
        }

        public User TryGetUser(string username, string password)
        {
            try
            {
                var user = this._usersService.Get(username, password);
                return user;
            }
            catch (EntityNotFoundException notFound)
            {
                throw new UnauthorizedOperationException(notFound.Message);
            }
        }
    }
}
