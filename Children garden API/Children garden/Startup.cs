using Children_garden.APIControllers.Helpers;
using Children_garden_Data;
using Children_garden_Data.Models.Mappers;
using Children_garden_Data.Repositories;
using Children_garden_Data.Repositories.Contracts;
using Children_garden_Services;
using Children_garden_Services.Contracts;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Children_garden
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Children_garden", Version = "v1" });
            });

            var connectionString = @"Server=DESKTOP-0CKJ40C;Database=ChildrenGardenDb;Trusted_Connection=True";

            services.AddDbContext<ChildrenGardenDbContext>(options => options.UseSqlServer(connectionString));
            //services.AddDbContext<ChildrenGardenDbContext>(options =>
            //options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            // Repositories
            services.AddScoped<IChildrenRepository, ChildrenRepository>();
            services.AddScoped<IChildrenGardensRepository, ChildrenGardensRepository>();
            services.AddScoped<IUsersRepository, UsersRepository>();
            //services.AddScoped<IGroupsRepository, GroupsRepository>();
            //services.AddScoped<IToysRepository, ToysRepository>();

            // Services
            services.AddScoped<IChildrenService, ChildrenService>();
            services.AddScoped<IChildrenGardensService, ChildrenGardensService>();
            services.AddScoped<IUsersService, UsersService>();
            //services.AddScoped<IGroupsService, GroupsService>();
            //services.AddScoped<IToysService, ToysService>();

            // Helpers
            services.AddTransient<ChildMapper>();
            services.AddTransient<ChildrenGardenMapper>();
            services.AddTransient<AuthorizationHelper>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Children_garden v1"));
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
