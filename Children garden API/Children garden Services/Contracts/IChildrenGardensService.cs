﻿using Children_garden_Data.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Services.Contracts
{
    public interface IChildrenGardensService
    {
        Task<ChildrenGardenResponseDto> GetByIdAsync(int id);
        Task<ChildrenGardenResponseDto> CreateAsync(ChildrenGardenCreateDto childToCreate, string username);
        Task<ChildrenGardenResponseDto> UpdateAsync(ChildrenGardenUpdateDto childToUpdate, string username);
        Task DeleteAsync(int id, string username);
    }
}
