﻿using Children_garden_Data.Models;
using Children_garden_Data.Models.Dtos;
using Children_garden_Data.Models.QueryParams;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Services.Contracts
{
    public interface IUsersService
    {
        User Get(string username, string password);
        ICollection<UserResponseDto> FilterBy(UsersQueryParams queryParams);

        Task<User> GetByIdAsync(int id);

        Task<UserResponseDto> CreateAsync(UserCreateDto createDto);

        Task<UserResponseDto> UpdateAsync(UserUpdateDto updateDto);

        Task DeleteAsync(int id);
    }
}
