﻿using Children_garden_Data.Models;
using Children_garden_Data.Models.Dtos;
using Children_garden_Data.Models.QueryParams;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Services.Contracts
{
    public interface IChildrenService
    {
        Task<ChildResponseDto> GetByIdAsync(int id);
        ICollection<ChildResponseDto> FilterBy( ChildrenQueryParams queryParams);
        Task<ChildResponseDto> CreateAsync(ChildCreateDto childToCreate, string username);
        Task<ChildResponseDto> UpdateAsync(ChildUpdateDto childToUpdate, string username);
        Task DeleteAsync(int id, string username);
    }
}
