﻿using Children_garden_Data.Models.Dtos;
using Children_garden_Data.Models.Mappers;
using Children_garden_Data.Models.QueryParams;
using Children_garden_Data.Repositories.Contracts;
using Children_garden_Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Services
{
    public class ChildrenService : IChildrenService
    {
        private readonly IChildrenRepository _childrenRepository;
        private readonly ChildMapper _childMapper;

        public ChildrenService(IChildrenRepository childrenRepository, ChildMapper childMapper)
        {
            _childrenRepository = childrenRepository;
            _childMapper = childMapper;
        }

        public ICollection<ChildResponseDto> FilterBy(ChildrenQueryParams queryParams)
        {
            var children = _childrenRepository.FilterBy(queryParams);

            var childrenResponseDto = children.Select(ch => _childMapper.ConvertToResponseDto(ch));

            return childrenResponseDto.ToList(); ;
        }

        public async Task<ChildResponseDto> GetByIdAsync(int id)
        {
            var child = await _childrenRepository.GetByIdAsync(id);
            var childResponseDto = _childMapper.ConvertToResponseDto(child);

            return childResponseDto;
        }

        public async Task<ChildResponseDto> CreateAsync(ChildCreateDto childCreateDto, string username)
        {
            // Vzema modelite ot id-tata

            //var group = await _groupService.GetByIdAsync(childCreateDto.GroupId);
            //var childrenGarden = await _childrenGardenService.GetById(childCreateDto.ChildrenGardenId);

            var childToCreate = _childMapper.ConvertToModel(childCreateDto);

            var createdChild = await _childrenRepository.CreateAsync(childToCreate);

            var childResponseDto = _childMapper.ConvertToResponseDto(createdChild);

            return childResponseDto;
        }

        public async Task<ChildResponseDto> UpdateAsync(ChildUpdateDto childToUpdate, string username)
        {
            var child = _childMapper.ConvertToModel(childToUpdate);

            var updatedChild = await _childrenRepository.UpdateAsync(child);

            var childResponseDto = _childMapper.ConvertToResponseDto(updatedChild);

            return childResponseDto;
        }

        public async Task DeleteAsync(int id, string username)
        {
            await _childrenRepository.DeleteAsync(id);
        }
    }
}
