﻿using Children_garden_Data.Models.Dtos;
using Children_garden_Data.Models.Mappers;
using Children_garden_Data.Repositories.Contracts;
using Children_garden_Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Services
{
    public class ChildrenGardensService : IChildrenGardensService
    {
        private readonly IChildrenGardensRepository _childrenGardensRepository;
        private readonly ChildrenGardenMapper _childrenGardenMapper;

        public ChildrenGardensService(IChildrenGardensRepository childrenGardensRepository, ChildrenGardenMapper childrenGardenMapper)
        {
            _childrenGardensRepository = childrenGardensRepository;
            _childrenGardenMapper = childrenGardenMapper;
        }

        public Task<ChildrenGardenResponseDto> CreateAsync(ChildrenGardenCreateDto childToCreate, string username)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(int id, string username)
        {
            throw new NotImplementedException();
        }

        //public ICollection<ChildrenGardenResponseDto> FilterBy(ChildrenQueryParams queryParams)
        //{
        //    var children = _childrenRepository.FilterBy(queryParams);

        //    var childrenResponseDto = children.Select(ch => _childMapper.ConvertToResponseDto(ch));

        //    return childrenResponseDto.ToList(); ;
        //}

        public async Task<ChildrenGardenResponseDto> GetByIdAsync(int id)
        {
            var childrenGarden = await _childrenGardensRepository.GetByIdAsync(id);
            var childrenGardenResponseDto = _childrenGardenMapper.ConvertToResponseDto(childrenGarden);

            return childrenGardenResponseDto;
        }

        public Task<ChildrenGardenResponseDto> UpdateAsync(ChildrenGardenUpdateDto childToUpdate, string username)
        {
            throw new NotImplementedException();
        }

        //public async Task<ChildResponseDto> CreateAsync(ChildrenGardenCreateDto createDto, string username)
        //{

        //    // Vzema modelite ot id-tata

        //    //var group = await _groupService.GetByIdAsync(childCreateDto.GroupId);
        //    //var childrenGarden = await _childrenGardenService.GetById(childCreateDto.ChildrenGardenId);

        //    var childToCreate = _childMapper.ConvertToModel(childCreateDto);

        //    var createdChild = await _childrenRepository.CreateAsync(childToCreate);

        //    var childResponseDto = _childMapper.ConvertToResponseDto(createdChild);

        //    return childResponseDto;
        //}

        //public async Task<ChildResponseDto> UpdateAsync(ChildUpdateDto childToUpdate, string username)
        //{
        //    var child = _childMapper.ConvertToModel(childToUpdate);

        //    var updatedChild = await _childrenRepository.UpdateAsync(child);

        //    var childResponseDto = _childMapper.ConvertToResponseDto(updatedChild);

        //    return childResponseDto;
        //}

        //public async Task DeleteAsync(int id, string username)
        //{
        //    await _childrenRepository.DeleteAsync(id);
        //}

    }
}
