﻿using Children_garden_Data.Models;
using Children_garden_Data.Models.Dtos;
using Children_garden_Data.Models.Mappers;
using Children_garden_Data.Models.QueryParams;
using Children_garden_Data.Repositories.Contracts;
using Children_garden_Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Services
{
    public class UsersService : IUsersService
    {
        private readonly IUsersRepository _usersRepository;

        private readonly UserMapper _userMapper;

        public UsersService(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }


        public User Get(string username, string password)
        {
            return _usersRepository.Get(username, password);
        }

        public ICollection<UserResponseDto> FilterBy(UsersQueryParams queryParams)
        {
            var users = _usersRepository.FilterBy(queryParams);

            var usersResponseDto = users.Select(u => _userMapper.ConvertToResponseDto(u));

            return usersResponseDto.ToList();
        }

        public async Task<User> GetByIdAsync(int id)
        {
           return await _usersRepository.GetByIdAsync(id);
        }

        public async Task<UserResponseDto> CreateAsync(UserCreateDto createDto)
        {
            var user = _userMapper.ConvertToModel(createDto);
            var createdUser = await _usersRepository.CreateAsync(user);

            var userResponseDto = _userMapper.ConvertToResponseDto(createdUser);

            return userResponseDto;
        }

        public async Task<UserResponseDto> UpdateAsync(UserUpdateDto updateDto)
        {
            var user = _userMapper.ConvertToModel(updateDto);
            var updatedUser = await _usersRepository.UpdateAsync(user);
            var responseDto = _userMapper.ConvertToResponseDto(updatedUser);

            return responseDto;
        }

        public async Task DeleteAsync(int id)
        {
             await _usersRepository.DeleteAsync(id);
        }
    }
}
