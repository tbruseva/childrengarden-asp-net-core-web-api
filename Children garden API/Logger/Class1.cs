﻿using System;

namespace Logger
{
    public class Logger
    {
        private static readonly Logger _instance = new Logger();

        private Logger()
        {

        }

        public static Logger GetLogger()
        {
            return _instance;
        }

        public static void Log()
        { 

        }
    }
}
