﻿using Children_garden_Data.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Data.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MinLength(2), MaxLength(25)]
        public string Name { get; set; }

        [Required]
        [MinLength(2), MaxLength(15)]
        public string UserName { get; set; }

        [Required]
        [MinLength(2), MaxLength(15)]
        public string Password { get; set; }

        public RoleType Role { get; set; }

        public int? GroupId { get; set; }
        public Group Group { get; set; }

        public int? ChildrenGardenId { get; set; }
        public ChildrenGarden ChildrenGarden { get; set; }

        public bool IsDeleted { get; set; }
    }
}
