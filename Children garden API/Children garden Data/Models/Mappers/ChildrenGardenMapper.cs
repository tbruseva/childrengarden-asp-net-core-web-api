﻿using Children_garden_Data.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Data.Models.Mappers
{
    public class ChildrenGardenMapper
    {

        //public ChildrenGarden ConvertToModel(ChildrenGardenCreateDto createDto)
        //{ 
        //    var childrenGarden = new ChildrenGarden();
        //    childrenGarden.Name = createDto.Name;
        //    childrenGarden.NumberOfChildren = createDto.NumberOfChildren\;
        //    childrenGarden.Groups = 
        //}

        public ChildrenGardenResponseDto ConvertToResponseDto(ChildrenGarden model)
        {
            var responseDto = new ChildrenGardenResponseDto
            {
                Name = model.Name,
                NumberOfChildren = model.NumberOfChildren
            };

            foreach ( var group in model.Groups)
            {
                responseDto.GroupsNames.Add(group.Name);
            }

            foreach (var child in model.Children)
            {
                responseDto.ChildrenNames.Add(child.Name);
            }

            foreach (var user in model.Users)
            {
                responseDto.UsersNames.Add(user.Name);
            }

            return responseDto;
        }
    }
}
