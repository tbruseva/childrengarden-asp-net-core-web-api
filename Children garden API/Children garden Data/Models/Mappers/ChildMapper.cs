﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Children_garden_Data.Models.Dtos;

namespace Children_garden_Data.Models.Mappers
{
    public class ChildMapper
    {
        public Child ConvertToModel(ChildCreateDto childCreateDto)
        {
            var child = new Child
            {
                Name = childCreateDto.Name,
                Age = childCreateDto.Age,
                GroupId = childCreateDto.GroupId,
                ChildrenGardenId = (int)childCreateDto.ChildrenGardenId
            };

            return child;
        }

        public ChildResponseDto ConvertToResponseDto(Child child)
        {
            var childResponseDto = new ChildResponseDto
            {
                Id = child.Id,
                Name = child.Name,
                Age = child.Age,
                GroupId = child.GroupId,
                ChildrenGardenId = child.ChildrenGardenId,
                PersonalToys = new List<string>(),
                IsSick = child.IsSick,
                HasChangedTheGroup = child.HasLeftTheGroup,
                HasLeftTheGarden = child.HasChangedTheGarden
            };

                foreach (var toy in child.PersonalToys)
                {
                    childResponseDto.PersonalToys.Add(toy.Name);
                }

            return childResponseDto;
        }

        public Child ConvertToModel(ChildUpdateDto childUpdateDto)
        {
            var child = new Child
            {
                Name = childUpdateDto.Name,
                Age = childUpdateDto.Age,
                GroupId = childUpdateDto.GroupId,
                ChildrenGardenId = childUpdateDto.ChildrenGardenId,
                PersonalToys = new List<Toy>(),
                IsSick = childUpdateDto.IsSick,
                HasLeftTheGroup = childUpdateDto.HasLeftTheGroup,
                HasChangedTheGarden = childUpdateDto.HasChangedTheGarden
            };
            foreach (var toy in childUpdateDto.PersonalToys)
            {
                child.PersonalToys.Add(toy);
            }

            return child;
        }

    }
}
