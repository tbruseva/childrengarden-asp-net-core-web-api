﻿using Children_garden_Data.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Data.Models.Mappers
{
    public class UserMapper
    {
        public UserResponseDto ConvertToResponseDto(User user)
        {
            var responseDto = new UserResponseDto();
            responseDto.Id = user.Id;
            responseDto.Name = user.Name;
            responseDto.UserName = user.UserName;
            responseDto.Password = user.Password;
            responseDto.Role = user.Role;
            responseDto.GroupId = (int)user.GroupId;
            responseDto.ChildrenGardenId = (int)user.ChildrenGardenId;
            responseDto.IsDeleted = user.IsDeleted;

            return responseDto;
        }

        public User ConvertToModel(UserCreateDto createDto)
        {
            var user = new User();
            user.Name = createDto.Name;
            user.UserName = createDto.UserName;
            user.Password = createDto.Password;
            user.GroupId = createDto.GroupId;
            user.ChildrenGardenId = createDto.ChildrenGardenId;

            return user;
        }

        public User ConvertToModel(UserUpdateDto updateDto)
        {
            var user = new User();
            user.Id = updateDto.Id;
            user.Name = updateDto.Name;
            user.UserName = updateDto.UserName;
            user.Password = updateDto.Password;
            user.GroupId = updateDto.GroupId;
            user.ChildrenGardenId = updateDto.ChildrenGardenId;

            return user;
        }
    }
}
