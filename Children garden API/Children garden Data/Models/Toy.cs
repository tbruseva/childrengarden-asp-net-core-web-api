﻿using System.ComponentModel.DataAnnotations;

namespace Children_garden_Data.Models
{
    public class Toy
    {

        [Key]
        public int Id { get; set; }

        [Required]
        [MinLength(2), MaxLength(20)]
        public string Name { get; set; } 

        public int OwnerId { get; set; }
        public Child Owner { get; set; }

        public bool IsLostOrBroken { get; set; } = false;
    }
}