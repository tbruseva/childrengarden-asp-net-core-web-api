﻿using Children_garden_Data.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Data.Models.QueryParams
{
    public class UsersQueryParams
    {
        public string Name { get; set; }

        public RoleType Role { get; set; }

        public string GroupId { get; set; }

        public string GroupName { get; set; }

        public string ChildrenGardenId { get; set; }

        public string ChildrenGardenName { get; set; }
    }
}
