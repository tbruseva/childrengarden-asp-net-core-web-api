﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Data.Models.QueryParams
{
    public class ChildrenQueryParams
    {
        public string Name { get; set; }

        public string GroupId { get; set; }

        public string GroupName { get; set; }

        public string ChildrenGardenName { get; set; }

        public bool IsEmpty()
        {
            return string.IsNullOrEmpty(Name)
                && string.IsNullOrEmpty(GroupId)
                && string.IsNullOrEmpty(GroupName)
                && string.IsNullOrEmpty(ChildrenGardenName);
        }
    }
}
