﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Data.Models.Enums
{
    public enum RoleType
    {
        Teacher,
        Parent,
        Director,
        Admin
    }
}
