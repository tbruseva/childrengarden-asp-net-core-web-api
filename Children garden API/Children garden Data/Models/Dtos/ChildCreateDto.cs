﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Data.Models.Dtos
{
    public class ChildCreateDto
    {
        [Required]
        [MinLength(2), MaxLength(25)]
        public string Name { get; set; }

        [Required]
        public int Age { get; set; }

        [Required]
        public int GroupId { get; set; }

        [Required]
        public int? ChildrenGardenId { get; set; }

        public List<Toy> PersonalToys { get; set; }
    }
}
