﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Data.Models.Dtos
{
    public class ChildrenGardenCreateDto
    {
        [Required]
        [MinLength(2), MaxLength(15)]
        public string Name { get; set; }

        public int NumberOfChildren { get; set; }

        public List<string> GroupsNames { get; set; }

        public List<string> ChildrenNames { get; set; }

        public List<string> UsersNames { get; set; }
    }
}
