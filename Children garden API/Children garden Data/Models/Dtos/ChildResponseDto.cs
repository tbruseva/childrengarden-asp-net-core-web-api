﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Data.Models.Dtos
{
    public class ChildResponseDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Age { get; set; }

        public int GroupId { get; set; }

        public int ChildrenGardenId { get; set; }

        public List<string> PersonalToys { get; set; }

        public bool IsSick { get; set; }

        public bool HasChangedTheGroup { get; set; }

        public bool HasLeftTheGarden { get; set; }
    }
}
