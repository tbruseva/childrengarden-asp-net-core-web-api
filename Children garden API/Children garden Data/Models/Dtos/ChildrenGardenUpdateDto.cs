﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Data.Models.Dtos
{
    public class ChildrenGardenUpdateDto
    {
        [Required]
        [MinLength(2), MaxLength(15)]
        public string Name { get; set; }

        public int NumberOfChildren { get; set; }

        public List<Group> Groups { get; set; }

        public List<Child> Children { get; set; }

        public List<User> Users { get; set; }

        public bool IsDeleted { get; set; } = false;
    }
}
