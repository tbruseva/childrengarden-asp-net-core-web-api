﻿using Children_garden_Data.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Data.Models.Dtos
{
    public class UserResponseDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public RoleType Role { get; set; }

        public int GroupId { get; set; }

        public int ChildrenGardenId { get; set; }

        public bool IsDeleted { get; set; }
    }
}
