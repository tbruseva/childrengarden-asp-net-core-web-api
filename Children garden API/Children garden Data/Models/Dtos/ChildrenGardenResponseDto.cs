﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Data.Models.Dtos
{
    public class ChildrenGardenResponseDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int NumberOfChildren { get; set; }

        public List<string> GroupsNames { get; set; } = new List<string>();

        public List<string> ChildrenNames { get; set; } = new List<string>();

        public List<string> UsersNames { get; set; } = new List<string>();
    }
}
