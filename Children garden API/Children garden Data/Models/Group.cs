﻿using Children_garden_Data.Models.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Children_garden_Data.Models
{
    public class Group
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MinLength(2), MaxLength(15)]
        public string Name { get; set; }

        [Required]
        public int ChildrenGardenId { get; set; }
        public ChildrenGarden ChildrenGarden { get; set; }

        [Range(4, 30)]
        public int NumberOfChildren { get; set; }

        public List<Child> Children { get; set; }

       // public int TeachersId { get; set; }

        //[Required]
        public List<User> Users { get; set; }

        public bool IsDeleted { get; set; }
    }
}