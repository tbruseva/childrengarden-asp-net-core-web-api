﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Data.Models
{
    public class Child
    {

        [Key]
        public int Id { get; set; }

        [Required]
        [MinLength(2), MaxLength(25)]
        public string Name { get; set; }

        public int Age { get; set; }

        public int GroupId { get; set; }
        public Group Group { get; set; }

        public int ChildrenGardenId { get; set; }
        public ChildrenGarden ChildrenGarden { get; set; }

        public List<Toy> PersonalToys { get; set; } = new List<Toy>();

        public bool IsSick { get; set; } = false;

        public bool HasLeftTheGroup { get; set; } = false;

        public bool HasChangedTheGarden { get; set; } = false;

        public bool IsDeleted { get; set; } = false;
    }
}
