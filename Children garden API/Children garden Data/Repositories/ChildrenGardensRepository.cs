﻿using Children_garden_Data.Models;
using Children_garden_Data.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Data.Repositories
{
    public class ChildrenGardensRepository : IChildrenGardensRepository
    {
        private readonly ChildrenGardenDbContext _dbContext;

        public ChildrenGardensRepository(ChildrenGardenDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public Task<ChildrenGarden> CreateAsync(ChildrenGarden garden)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<ChildrenGarden> GetAll()
        {
            throw new NotImplementedException();
        }

        public Task<ChildrenGarden> GetByIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<ChildrenGarden> GetByNameAsync(string gardenName)
        {
            throw new NotImplementedException();
        }

        public Task<ChildrenGarden> UpdateAsync(ChildrenGarden garden)
        {
            throw new NotImplementedException();
        }
    }
}
