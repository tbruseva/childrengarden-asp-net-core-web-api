﻿using Children_garden_Data.Exceptions;
using Children_garden_Data.Models;
using Children_garden_Data.Models.QueryParams;
using Children_garden_Data.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Data.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private readonly ChildrenGardenDbContext _dbContext;

        public UsersRepository(ChildrenGardenDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public User Get(string username, string password)
        {
            var user = this._dbContext.Users.Where(u => u.UserName == username && u.Password == password).FirstOrDefault();

            return user ?? throw new EntityNotFoundException("User doesn't exist");
        }

        public IQueryable<User> FilterBy(UsersQueryParams queryParams)
        {
            var users = this.GetAllWithoutIncludes();

            users = this.FilterByName(users, queryParams);
            users = this.FilterByGroupId(users, queryParams);
            users = this.FilterByGroupName(users, queryParams);
            users = this.FilterByChildrenGardenId(users, queryParams);
            users = this.FilterByChildrenGardenName(users, queryParams);

            users = users.Include(ch => ch.Group)
                               .Include(ch => ch.ChildrenGarden);

            return users;
        }

        public IQueryable<User> GetAllWithoutIncludes()
        {
            return _dbContext.Users.AsQueryable();
        }

        public IQueryable<User> FilterByName(IQueryable<User> users, UsersQueryParams queryParams)
        {
            if (queryParams.Name == null)
            {
                return users;
            }

            return users.Where(u => u.Name == queryParams.Name).AsQueryable();
        }

        public IQueryable<User> FilterByGroupId(IQueryable<User> users, UsersQueryParams queryParams)
        {
            if (queryParams.GroupId == null)
            {
                return users;
            }

            return users.Where(u => u.GroupId == int.Parse(queryParams.GroupId)).AsQueryable();
        }

        public IQueryable<User> FilterByGroupName(IQueryable<User> users, UsersQueryParams queryParams)
        {
            if (queryParams.GroupName == null)
            {
                return users;
            }

            return users.Where(u => u.Group.Name == queryParams.GroupName).AsQueryable();
        }

        public IQueryable<User> FilterByChildrenGardenId(IQueryable<User> users, UsersQueryParams queryParams)
        {
            if (queryParams.ChildrenGardenId == null)
            {
                return users;
            }

            return users.Where(u => u.ChildrenGardenId == int.Parse(queryParams.ChildrenGardenId)).AsQueryable();
        }

        public IQueryable<User> FilterByChildrenGardenName(IQueryable<User> users, UsersQueryParams queryParams)
        {
            if (queryParams.ChildrenGardenName == null)
            {
                return users;
            }

            return users.Where(u => u.ChildrenGarden.Name == queryParams.ChildrenGardenName).AsQueryable();
        }

        public async Task<User> GetByIdAsync(int id)
        {
            var user = _dbContext.Users.Where(u => u.Id == id).FirstOrDefault();

            if (user == null)
            {
                throw new EntityNotFoundException("User doesn't exist!");
            }

            return user;
        }

        public async Task<User> CreateAsync(User user)
        {
            try
            {
                var createdUser = await _dbContext.Users.AddAsync(user);
                await _dbContext.SaveChangesAsync();

                return createdUser.Entity;
            }
            catch
            {
                throw new InvalidInputException("User was not created!");
            }
        }

        public async Task<User> UpdateAsync(User userToUpdate)
        {
            var user = _dbContext.Users.FirstOrDefault(u => u.Id == userToUpdate.Id);
            user.Name = userToUpdate.Name;
            user.UserName = userToUpdate.UserName;
            user.Password = userToUpdate.Password;
            user.GroupId = userToUpdate.GroupId;
            user.Group = userToUpdate.Group;
            user.ChildrenGardenId = userToUpdate.ChildrenGardenId;
            user.ChildrenGarden = userToUpdate.ChildrenGarden;

            return user;
        }

        public async Task DeleteAsync(int id)
        {
            var user = await this.GetByIdAsync(id);
            user.IsDeleted = true;
           // _dbContext.Update(user);
            await _dbContext.SaveChangesAsync();
        }
    }
}
