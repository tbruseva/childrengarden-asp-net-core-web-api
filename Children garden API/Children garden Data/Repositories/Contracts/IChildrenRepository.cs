﻿using Children_garden_Data.Models;
using Children_garden_Data.Models.Dtos;
using Children_garden_Data.Models.QueryParams;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Data.Repositories.Contracts
{
    public interface IChildrenRepository
    {
        IQueryable<Child> GetAllChildren();
        IQueryable<Child> GetAllChildrenWithoutIncludes();
        IQueryable<Child> FilterBy(ChildrenQueryParams queryParams);
        IQueryable<Child> FilterByName(IQueryable<Child> children, ChildrenQueryParams queryParams);
        IQueryable<Child> FilterByGroupId(IQueryable<Child> children, ChildrenQueryParams queryParams);
        IQueryable<Child> FilterByGroupName(IQueryable<Child> children, ChildrenQueryParams queryParams);
        IQueryable<Child> FilterByChildrenGardenName(IQueryable<Child> children, ChildrenQueryParams queryParams);


        Task<Child> GetByIdAsync(int id);

        Task<Child> CreateAsync(Child child);

        Task<Child> GetByNameAsync(string name);

        Task<Child> UpdateAsync(Child child);

        Task DeleteAsync(int id);
    }
}
