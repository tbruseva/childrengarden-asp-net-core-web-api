﻿using Children_garden_Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Data.Repositories.Contracts
{
    public interface IChildrenGardensRepository
    {
        IQueryable<ChildrenGarden> GetAll();

        Task<ChildrenGarden> GetByIdAsync(int id);

        Task<ChildrenGarden> CreateAsync(ChildrenGarden garden);

        Task<ChildrenGarden> GetByNameAsync(string gardenName);

        Task<ChildrenGarden> UpdateAsync(ChildrenGarden garden);

        Task DeleteAsync(int id);
    }
}
