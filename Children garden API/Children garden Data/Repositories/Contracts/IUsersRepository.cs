﻿using Children_garden_Data.Models;
using Children_garden_Data.Models.Dtos;
using Children_garden_Data.Models.QueryParams;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Data.Repositories.Contracts
{
   public interface IUsersRepository
    {
        User Get(string username, string password);

        IQueryable<User> FilterBy(UsersQueryParams queryParams);

        Task<User> GetByIdAsync(int id);

        Task<User> CreateAsync(User user);

        Task<User> UpdateAsync(User user);

        Task DeleteAsync(int id);

    }
}
