﻿using Children_garden_Data.Exceptions;
using Children_garden_Data.Models;
using Children_garden_Data.Models.QueryParams;
using Children_garden_Data.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Data.Repositories
{
    public class ChildrenRepository : IChildrenRepository
    {
        private readonly ChildrenGardenDbContext _dbContext;

        public ChildrenRepository(ChildrenGardenDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public IQueryable<Child> GetAllChildren()
        {
            return _dbContext.Children.Include(ch => ch.Group)
                                   .Include(ch => ch.ChildrenGarden)
                                   .Include(ch => ch.PersonalToys).AsQueryable();
        }

        public IQueryable<Child> GetAllChildrenWithoutIncludes()
        {
            return _dbContext.Children.AsQueryable();
        }

        public IQueryable<Child> FilterBy(ChildrenQueryParams queryParams)
        {
            var children = this.GetAllChildrenWithoutIncludes();

            children = this.FilterByName(children, queryParams);
            children = this.FilterByGroupId(children, queryParams);
            children = this.FilterByGroupName(children, queryParams);
            children = this.FilterByChildrenGardenName(children, queryParams);

            children = children.Include(ch => ch.Group)
                               .Include(ch => ch.ChildrenGarden)
                               .Include(ch => ch.PersonalToys);

            return children;
        }

        public IQueryable<Child> FilterByName(IQueryable<Child> children, ChildrenQueryParams queryParams)
        {
            if (queryParams.Name == null)
            {
                return children;
            }

            return children.Where(ch => ch.Name == queryParams.Name);
        }

        public IQueryable<Child> FilterByGroupId(IQueryable<Child> children, ChildrenQueryParams queryParams)
        {
            if (queryParams.GroupId == null || queryParams.GroupId == "0")
            {
                return children;
            }

            return children.Where(ch => ch.GroupId == int.Parse(queryParams.GroupId));
        }

        public IQueryable<Child> FilterByGroupName(IQueryable<Child> children, ChildrenQueryParams queryParams)
        {
            if (queryParams.GroupName == null)
            {
                return children;
            }

            return children.Where(ch => ch.Group.Name == queryParams.Name);
        }

        public IQueryable<Child> FilterByChildrenGardenName(IQueryable<Child> children, ChildrenQueryParams queryParams)
        {
            if (queryParams.ChildrenGardenName == null)
            {
                return children;
            }

            return children.Where(ch => ch.ChildrenGarden.Name == queryParams.ChildrenGardenName);
        }
        public async Task<Child> GetByIdAsync(int id)
        {
            var child = await _dbContext.Children.FirstOrDefaultAsync(i => i.Id == id);

            return child ?? throw new EntityNotFoundException($"Child with Id {id} does not exist!");
        }


        public async Task<Child> CreateAsync(Child child)
        {
            var createdChild = await _dbContext.Children.AddAsync(child);
            await _dbContext.SaveChangesAsync();

            return createdChild.Entity;
        }

        public async Task<Child> GetByNameAsync(string name)
        {
            // var child =  _dbContext.Children.Where(c => c.Name == name).FirstOrDefault();
            var child = await _dbContext.Children.FirstOrDefaultAsync(c => c.Name == name);
            return child;
        }

        public async Task<Child> UpdateAsync(Child childToUpdate)
        {
            var updatedChild = await this.GetByNameAsync(childToUpdate.Name);
            updatedChild.Name = childToUpdate.Name;
            updatedChild.Age = childToUpdate.Age;
            updatedChild.GroupId = childToUpdate.GroupId;
            updatedChild.Group = childToUpdate.Group;
            updatedChild.ChildrenGardenId = childToUpdate.ChildrenGardenId;
            updatedChild.ChildrenGarden = childToUpdate.ChildrenGarden;
            updatedChild.IsSick = childToUpdate.IsSick;
            updatedChild.HasLeftTheGroup = childToUpdate.HasLeftTheGroup;
            updatedChild.HasChangedTheGarden = childToUpdate.HasChangedTheGarden;

            await _dbContext.SaveChangesAsync();

            // return updatedChild;

           return await this.GetByIdAsync(updatedChild.Id);
        }

        public async Task DeleteAsync(int id)
        {
            var child = await GetByIdAsync(id);
            child.IsDeleted = true;
            _dbContext.Update(child);
            await _dbContext.SaveChangesAsync();
        }
    }
}
