﻿using Children_garden_Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Children_garden_Data
{
    public class ChildrenGardenDbContext : DbContext
    {
        public ChildrenGardenDbContext(DbContextOptions<ChildrenGardenDbContext> options)
            : base(options)
        {
        }

        public DbSet<Child> Children { get; set; }
        public DbSet<ChildrenGarden> ChildrenGardens { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Toy> Toys { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Child>()
            .HasOne<Group>(ch => ch.Group)
            .WithMany(g => g.Children)
            .HasForeignKey(s => s.GroupId)
            .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Child>()
            .HasOne<ChildrenGarden>(ch => ch.ChildrenGarden)
            .WithMany(chG => chG.Children)
            .HasForeignKey(s => s.ChildrenGardenId)
            .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Group>()
                .HasOne<ChildrenGarden>(g => g.ChildrenGarden)
                .WithMany(chG => chG.Groups)
                .HasForeignKey(g => g.ChildrenGardenId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Toy>()
                .HasOne<Child>(t => t.Owner)
                .WithMany(ch => ch.PersonalToys)
                .HasForeignKey(g => g.OwnerId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<User>()
                .HasOne<Group>(u => u.Group)
                .WithMany(g => g.Users)
                .HasForeignKey(g => g.GroupId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<User>()
                .HasOne<ChildrenGarden>(u => u.ChildrenGarden)
                .WithMany(chG => chG.Users)
                .HasForeignKey(g => g.ChildrenGardenId)
                .OnDelete(DeleteBehavior.Restrict);

            var users = new List<User>();
            users.Add(new User
            {
                Id = 1,
                Name = "Tony",
                UserName = "tbruseva",
                Password = "123456",
                Role = Models.Enums.RoleType.Admin
            });
            users.Add(new User
            {
                Id = 2,
                Name = "Gospoja1",
                UserName = "gospoja1",
                Password = "123456",
                GroupId = 1,
                ChildrenGardenId = 1,
                Role = Models.Enums.RoleType.Teacher
            });
            users.Add(new User
            {
                Id = 3,
                Name = "Gospoja2",
                UserName = "gospoja2",
                Password = "123456",
                GroupId = 1,
                ChildrenGardenId = 1,
                Role = Models.Enums.RoleType.Teacher
            });
            users.Add(new User
            {
                Id = 4,
                Name = "Gospoja3",
                UserName = "gospoja3",
                Password = "123456",
                GroupId = 2,
                ChildrenGardenId = 2,
                Role = Models.Enums.RoleType.Teacher
            });
            users.Add(new User
            {
                Id = 5,
                Name = "Gospoja4",
                UserName = "gospoja4",
                Password = "123456",
                GroupId = 2,
                ChildrenGardenId = 2,
                Role = Models.Enums.RoleType.Teacher
            });
            users.Add(new User
            {
                Id = 6,
                Name = "Roditel1",
                UserName = "Roditel1",
                Password = "123456",
                GroupId = 1,
                ChildrenGardenId = 1,
                Role = Models.Enums.RoleType.Parent
            });
            users.Add(new User
            {
                Id = 7,
                Name = "Direktor",
                UserName = "Direktor",
                Password = "123456",
                ChildrenGardenId = 1,
                Role = Models.Enums.RoleType.Director
            });
            modelBuilder.Entity<User>().HasData(users);

            var toys = new List<Toy>();
            toys.Add(new Toy
            {
                Id = 1,
                Name = "doll",
                OwnerId = 1,
                IsLostOrBroken = false
            });
            toys.Add(new Toy
            {
                Id = 2,
                Name = "car",
                OwnerId = 2,
                IsLostOrBroken = false
            });
            modelBuilder.Entity<Toy>().HasData(toys);

            var children = new List<Child>();
            children.Add(new Child
            {
                Id = 1,
                Name = "Pepelqshka",
                Age = 3,
                GroupId = 1,
                ChildrenGardenId = 1
            });
            children.Add(new Child
            {
                Id = 2,
                Name = "Malkiqt princ",
                Age = 3,
                GroupId = 1,
                ChildrenGardenId = 1
            });
            children.Add(new Child
            {
                Id = 3,
                Name = "Razboinik",
                Age = 3,
                GroupId = 1,
                ChildrenGardenId = 1,
                IsSick = true
            });
            children.Add(new Child
            {
                Id = 4,
                Name = "Spqshtata krasavica",
                Age = 3,
                GroupId = 1,
                ChildrenGardenId = 1
            });
            children.Add(new Child
            {
                Id = 5,
                Name = "Desi",
                Age = 4,
                GroupId = 2,
                ChildrenGardenId = 2
            });
            children.Add(new Child
            {
                Id = 6,
                Name = "Goshko",
                Age = 4,
                GroupId = 2,
                ChildrenGardenId = 2
            });
            modelBuilder.Entity<Child>().HasData(children);

            var childrenGardens = new List<ChildrenGarden>();
            childrenGardens.Add(new ChildrenGarden
            {
                Id = 1,
                Name = "Slynce",
                NumberOfChildren = 15,
            });
            childrenGardens.Add(new ChildrenGarden
            {
                Id = 2,
                Name = "Dyga",
                NumberOfChildren = 10,
            });
            childrenGardens.Add(new ChildrenGarden
            {
                Id = 3,
                Name = "Zora",
                NumberOfChildren = 20,
            });
            modelBuilder.Entity<ChildrenGarden>().HasData(childrenGardens);

            var groups = new List<Group>();
            groups.Add(new Group
            {
                Id = 1,
                Name = "Bubolechka",
                ChildrenGardenId = 1,
                NumberOfChildren = 16
            });
            groups.Add(new Group
            {
                Id = 2,
                Name = "Zvezdichka",
                ChildrenGardenId = 2,
                NumberOfChildren = 20
            });
            modelBuilder.Entity<Group>().HasData(groups);

        }
    }
}