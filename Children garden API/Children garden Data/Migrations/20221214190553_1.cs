﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Children_garden_Data.Migrations
{
    public partial class _1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ChildrenGardens",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    NumberOfChildren = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChildrenGardens", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Groups",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    ChildrenGardenId = table.Column<int>(type: "int", nullable: false),
                    NumberOfChildren = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Groups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Groups_ChildrenGardens_ChildrenGardenId",
                        column: x => x.ChildrenGardenId,
                        principalTable: "ChildrenGardens",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Children",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(25)", maxLength: 25, nullable: false),
                    Age = table.Column<int>(type: "int", nullable: false),
                    GroupId = table.Column<int>(type: "int", nullable: false),
                    ChildrenGardenId = table.Column<int>(type: "int", nullable: false),
                    IsSick = table.Column<bool>(type: "bit", nullable: false),
                    HasLeftTheGroup = table.Column<bool>(type: "bit", nullable: false),
                    HasChangedTheGarden = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Children", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Children_ChildrenGardens_ChildrenGardenId",
                        column: x => x.ChildrenGardenId,
                        principalTable: "ChildrenGardens",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Children_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(25)", maxLength: 25, nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    Password = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    Role = table.Column<int>(type: "int", nullable: false),
                    GroupId = table.Column<int>(type: "int", nullable: true),
                    ChildrenGardenId = table.Column<int>(type: "int", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_ChildrenGardens_ChildrenGardenId",
                        column: x => x.ChildrenGardenId,
                        principalTable: "ChildrenGardens",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Toys",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    OwnerId = table.Column<int>(type: "int", nullable: false),
                    IsLostOrBroken = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Toys", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Toys_Children_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "Children",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "ChildrenGardens",
                columns: new[] { "Id", "IsDeleted", "Name", "NumberOfChildren" },
                values: new object[,]
                {
                    { 1, false, "Slynce", 15 },
                    { 2, false, "Dyga", 10 },
                    { 3, false, "Zora", 20 }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "ChildrenGardenId", "GroupId", "IsDeleted", "Name", "Password", "Role", "UserName" },
                values: new object[] { 1, null, null, false, "Tony", "123456", 3, "tbruseva" });

            migrationBuilder.InsertData(
                table: "Groups",
                columns: new[] { "Id", "ChildrenGardenId", "IsDeleted", "Name", "NumberOfChildren" },
                values: new object[] { 1, 1, false, "Bubolechka", 16 });

            migrationBuilder.InsertData(
                table: "Groups",
                columns: new[] { "Id", "ChildrenGardenId", "IsDeleted", "Name", "NumberOfChildren" },
                values: new object[] { 2, 2, false, "Zvezdichka", 20 });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "ChildrenGardenId", "GroupId", "IsDeleted", "Name", "Password", "Role", "UserName" },
                values: new object[] { 7, 1, null, false, "Direktor", "123456", 2, "Direktor" });

            migrationBuilder.InsertData(
                table: "Children",
                columns: new[] { "Id", "Age", "ChildrenGardenId", "GroupId", "HasChangedTheGarden", "HasLeftTheGroup", "IsDeleted", "IsSick", "Name" },
                values: new object[,]
                {
                    { 1, 3, 1, 1, false, false, false, false, "Pepelqshka" },
                    { 2, 3, 1, 1, false, false, false, false, "Malkiqt princ" },
                    { 3, 3, 1, 1, false, false, false, true, "Razboinik" },
                    { 4, 3, 1, 1, false, false, false, false, "Spqshtata krasavica" },
                    { 5, 4, 2, 2, false, false, false, false, "Desi" },
                    { 6, 4, 2, 2, false, false, false, false, "Goshko" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "ChildrenGardenId", "GroupId", "IsDeleted", "Name", "Password", "Role", "UserName" },
                values: new object[,]
                {
                    { 2, 1, 1, false, "Gospoja1", "123456", 0, "gospoja1" },
                    { 3, 1, 1, false, "Gospoja2", "123456", 0, "gospoja2" },
                    { 6, 1, 1, false, "Roditel1", "123456", 1, "Roditel1" },
                    { 4, 2, 2, false, "Gospoja3", "123456", 0, "gospoja3" },
                    { 5, 2, 2, false, "Gospoja4", "123456", 0, "gospoja4" }
                });

            migrationBuilder.InsertData(
                table: "Toys",
                columns: new[] { "Id", "IsLostOrBroken", "Name", "OwnerId" },
                values: new object[] { 1, false, "doll", 1 });

            migrationBuilder.InsertData(
                table: "Toys",
                columns: new[] { "Id", "IsLostOrBroken", "Name", "OwnerId" },
                values: new object[] { 2, false, "car", 2 });

            migrationBuilder.CreateIndex(
                name: "IX_Children_ChildrenGardenId",
                table: "Children",
                column: "ChildrenGardenId");

            migrationBuilder.CreateIndex(
                name: "IX_Children_GroupId",
                table: "Children",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Groups_ChildrenGardenId",
                table: "Groups",
                column: "ChildrenGardenId");

            migrationBuilder.CreateIndex(
                name: "IX_Toys_OwnerId",
                table: "Toys",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_ChildrenGardenId",
                table: "Users",
                column: "ChildrenGardenId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_GroupId",
                table: "Users",
                column: "GroupId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Toys");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Children");

            migrationBuilder.DropTable(
                name: "Groups");

            migrationBuilder.DropTable(
                name: "ChildrenGardens");
        }
    }
}
